import subprocess
import os
from os import path
import sys
import shutil
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('COVID19')

def get_project(project_to_clone):
    cwd = os.getcwd()
    logger.info(f"cloning git project: '{project_to_clone}'")
    proj_name=os.path.basename(os.path.splitext(project_to_clone)[0])
    proj_dir = path.join(cwd, proj_name)
    logger.info(f"cloning git project '{proj_name}' to directory '{proj_dir}'")
    if path.isdir(proj_dir):
        logger.info(f"directory '{proj_dir}' exists... performing git pull:")
        os.chdir(proj_dir)
        subprocess.run(["git", "pull"])
        os.chdir("..")
        logger.info(f"pulling done")
    else:
        subprocess.run(["git", "clone", project_to_clone, proj_name])
        logger.info(f"cloning done")
    return proj_dir


def get_covid_data(name, proj_dir, countries):

    logger.info(f"Reading data '{name}' for countries {countries}")

    data = pd.read_csv(path.join(proj_dir, 
                                 'csse_covid_19_data', 
                                 'csse_covid_19_time_series', 
                                 f'time_series_covid19_{name}.csv')).to_numpy()

    countries_all = list(data[:,1])
    assert set(countries_all) >= set(countries)

    data = data[:, 4:] # remove the first four excel columns

    # last country index, since some countries include regions
    countries_indices = [len(countries_all) - 1 - countries_all[::-1].index(c) for c in countries]
    data = data[countries_indices, :]
    return data

def get_shift(data, threshold):
    return [np.argmin(np.abs(data[i,:]-threshold)) for i in range(data.shape[0])]

if __name__ == "__main__":

    proj_dir = get_project('https://github.com/CSSEGISandData/COVID-19.git')
    # countries = ['Israel',	'Switzerland',	'Austria',	'Italy',	'Spain',	'United Kingdom',	'Japan',	'Netherlands',	'Germany',                 'Korea, South', 'US']
    countries = ['Israel', 'Italy',	'Spain', 'France', 'United Kingdom', 'Korea, South', 'Germany', 'Czechia','US']

    confirmed_global = get_covid_data('confirmed_global', proj_dir, countries)
    deaths_global    = get_covid_data('deaths_global',    proj_dir, countries)
    shift = get_shift(data=confirmed_global, threshold=100)

    colors = cm.rainbow(np.linspace(0,1,len(countries)))
    markers = [u'o', u'v', u'+', u'<', u'>', u'8', u's', u'x', u'*', u'h', u'H', u'D', u'd']

    for i, c in reversed(list(enumerate(countries))):
        color = colors[i]
        plt.plot(confirmed_global[i, shift[i]:], marker=markers[i], markersize=3.5, c=color, label=f"{c} {confirmed_global[i, -1]} ({deaths_global[i, -1]})")
        plt.plot(deaths_global[i, shift[i]:], marker=markers[i], markersize=3.5, c=color, ls='--')

    plt.legend(fontsize=8, loc='upper left')
    plt.grid()
    plt.yscale("log")
    plt.xlim(xmin=0)
    plt.ylim(ymin=1.)
    # plt.xlabel("זמן מהתפרצות ]ימים["[::-1], fontsize=12)
    plt.xlabel("זמן מ-001 חולים מאומתים ]ימים["[::-1], fontsize=12)
    plt.ylabel("מקרים מאומתים )מוות("[::-1], fontsize=12)
    plt.savefig("fig.pdf")
    plt.savefig("fig.png")
    plt.show()